package main

import (
	"bufio"
	"context"
	"fmt"
	consul "github.com/hashicorp/consul/api"
	"gitlab.com/Vakoto/museums-tour-tools/model"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
	"google.golang.org/grpc/peer"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	config := consul.DefaultConfig()
	config.Address = consulAddress
	consulClient, err := consul.NewClient(config)

	services, _, err := consulClient.Health().Service("dispatcher", "", true, nil)
	if err != nil {
		log.Fatalf("Can't get alive dispatcher services: %v", err)
	}

	addresses := make([]string, 0)
	for _, item := range services {
		addr := item.Service.Address + ":" + strconv.Itoa(item.Service.Port)
		addresses = append(addresses, addr)
	}

	if len(addresses) == 0 {
		log.Fatalf("Failed to find healthy dispatcher services")
	}
	log.Printf("Received services: %v", addresses)

	rand.Seed(time.Now().Unix())
	randomIndex := rand.Intn(len(addresses))
	grpcAddr := addresses[randomIndex]
	grpcConn, err := grpc.Dial(
		grpcAddr,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name))

	if err != nil {
		log.Fatalf("Failed to dial: %v", err)
	}
	defer grpcConn.Close()

	c := model.NewMuseumDispatcherClient(grpcConn)
	if err != nil {
		log.Fatalf("Can't connect to grpc: %v", err)
	}
	defer grpcConn.Close()

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	var p peer.Peer
	ctx = peer.NewContext(ctx, &p)
	log.Printf("[%s] GRPC: sent request", grpcAddr)
	reply, err := c.FetchMuseums(ctx, &model.GetMuseumsRequest{})
	if err != nil {
		log.Printf("[%s] GRPC: could not get reply: %v", grpcAddr, err)
		return
	}
	log.Printf("[%s] GRPC: response content: Museums: %v\tDistance: %v", grpcAddr, reply.Museums)

	museums := reply.Museums
	for _, museum := range museums {
		fmt.Printf("%d\t%s\n", museum.Id, museum.Name)
	}

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	ids := strings.Split(scanner.Text(), " ")

	selectedMuseumIds := make([]int32, 0)
	for _, str := range ids {
		id, err := strconv.ParseInt(str, 10, 32)
		if err != nil {
			log.Printf("[%s] GRPC: failed to read selected ids %v", grpcAddr, err)
			return
		}

		selectedId := int32(id)
		selectedMuseumIds = append(selectedMuseumIds, selectedId)
	}

	log.Printf("[%s] GRPC: sent request", grpcAddr)
	getPathReply, err := c.GetPath(ctx, &model.GetPathRequest { MuseumIds: selectedMuseumIds})

	if err != nil {
		log.Printf("[%s] GRPC: could not get reply: %v", grpcAddr, err)
		return
	}
	log.Printf("[%s] GRPC: response content: Museums: %v\tDistance: %v", grpcAddr, getPathReply.Path, getPathReply.Distance)

	fmt.Print("start -> ")
	for _, id := range getPathReply.Path {
		fmt.Printf("%s -> ", museums[id].Name)
	}
	fmt.Print("finish\n")
}
